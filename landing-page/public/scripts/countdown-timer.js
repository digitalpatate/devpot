// Set the date we're counting down to
var countDownDate = new Date("Dec 20, 2019 23:55:00").getTime();

function formatNumber(value) {
    return '' + (value < 10 ? '0' + value : value);
}

function updateCountdown(days, hours, minutes, seconds) {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if (distance >= 0) {
        document.getElementById("cd-d").innerHTML = formatNumber(days) + " D";
        document.getElementById("cd-h").innerHTML = formatNumber(hours) + " H";
        document.getElementById("cd-m").innerHTML = formatNumber(minutes) + " M";
        document.getElementById("cd-s").innerHTML = formatNumber(seconds) + " S";
    }

    return distance
}

// Update the count down every 1 second
var x = setInterval(function () {
    if (updateCountdown() < 0) {
        clearInterval(x);
    }
}, 1000);

updateCountdown()