# DevPot - Back-End

All micro-services will talk to each other through [RESTful](https://restfulapi.net) routes. And the authorization will use [JWT](https://jwt.io/introduction/) technology.


## Cycle 1 - basic infrastructure :

It's the first step of the project, the creation of the infrastructure for the back-end , the front-end and the devops part.

We will create a "main" back-end service that will only return project and related devlogs data.

To complete this cycle we'll need two collections
- Project
- Devlog

### References

A project has many devlog, so in the project collection we'll have a field that refer to a devlog through his id

In order to make the front-end access to back-end we'll create theses routes all other will return a 404 error.

#### Routes

| url                          | method |
| ---------------------------- | ------ |
| /projects                    | GET    |
| /projects/:projectId         | GET    |
| /projects/:projectId/devlogs | GET    |
| /devlogs/:projectId          | GET    |

## Cycle 2 - user authentication

The purpose of the second cycle is adding user authentication to allow someone create a devpot account. 
We'll create a separated and dedicated service, the "auth" service.

To make this work, we'll need a new "user" collection and the below routes


#### Routes

| url         | method |
| ----------- | ------ |
| /login      | POST   |
| /register   | POST   |
| /reset      | POST   |
| /validation | POST   |

We will use the passport.js library to handle these login and register actions with the "local" strategy

## Cycle 3 - user-project ownership

Now we need to add to the main-service's project collection a new field that reference the user'id from the auth service.

And then we will add these new actions to the main-service.
#### Routes


| url                          | method |
| ---------------------------- | ------ |
| /projects                    | POST   |
| /projects/:projectId         | UPDATE |
| /projects/:projectId         | DELETE |
| /projects/:projectId/devlogs | POST   |
| /devlogs/:devlogId           | UPDATE |
| /devlogs/:devlogId           | DELETE |

These routes will all have an express middleware that will check the presence and the validity of the JWT authentication token and if the token owner have the access to the resource.

## Cycle 4 - Social medias

In the step, we want to add some social media informations to display on the project page. For example how many contributor is on the github repo, how many patreon subscriber the project has.

To access this information we will talk to their public API. But the problem is that some of these API have a request limitation in a day. So to prevent the situation that we don't have any request allowed. We will create a service for each social media that will periodically save  the infos and then it's the main-service that will request on the social media service to have the informations.

## Cycle 5 - search and filter

### Service
#### main-service

Add a new label to the project
 - A collection of tags

And then update the `GET /projects` to handle filters

#### Routes
| url                              | method |
| -------------------------------- | ------ |
| /projects?tag=[...]&title=[...]] | GET    |

## Cycle 6 - Organization

The final step is to add an organization system like github.
The user can create an organization and add to it some project and invite other user.

So we will add a new collection `organization` to the main-service and add these following routes

#### Routes

| url                          | method |
| ---------------------------- | ------ |
| /organizations                    | GET,POST|
| /organizations/:projectId         | GET,PUT,DELETE |
| /organizations/:projectId/members | PUT    |

To transfer or create project in an organization we'll update the project-user reference to have a [Dynamic reference](https://mongoosejs.com/docs/populate.html#dynamic-ref) on the owner of a project. So It can be a user or an organization

