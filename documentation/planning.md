# DevPot - Cycles Planning

A simple list of functional requirement for each project cycle

## Cycle 1 - Basic infrastructure :

- Create the project page.
- Display the list of devlogs
- Create the devlog page (markdown or BB code).

## Cycle 2 - User authentication:

- A user can :
  - register/login
  - reset password action
  - email verification
  - update user informations

## Cycle 3 - User-project ownership

- A user can .
  - own and create projects
  - update
  - delete
- A user can :
  - create devlogs (articles)
  - update
  - delete
## Cycle 4 - Social medias
- A user can
  -  link/unlink his social medias
  - display/create publications

## Cycle 5 - Advance infrastructure

- Creation of :
  - home page
  - like system
  - project search
  - dashboard

## Cycle 6 - Organisation 

- A user can :
  -  create and own an organization 
  - update informations
  - manage members
    - add
    - remove
    - update role
  - delete
  - transfer project ownership
  -  browse through the organisations 