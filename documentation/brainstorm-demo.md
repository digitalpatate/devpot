# Demo presentation

## Introduction

- Presentation of the landing page, where  the featured, random and most viewed projects will be displayed.
- Explanation of what's  going on. What's the application and what's being displayed .
- Description of the application's usage  (what can we do). 
- Story behind the idea (the reason/motivation).

## Projects

- Presentation of the projects page and searching  feature.
- Selection of a project as an example in order to display the project's description page.
- Description of  what's a project and their content (devlogs).
- Presentation of  the devlogs and their purpose.
- Display of  multiple devlogs with different type of bodies (containing either  videos, images or texts).

## Organisations

- Presentation of the Organisations page. 
- Explanation of what can we find at an organisation's page.
- Description of what's an organisation and it's  difference between  a user account.
- Display of  the search feature and organisation selection.

## Users

- Display of an user's profile page (while disconnect).
- Description of  what can we find at the user's profile page (while disconnect) .
- Presentation of the user's dashboard (Connect with an user ).
- Explanation of  how we can see his organisations, projects an favourites.
- Creation of a new project and devlog.
- Creation of an organisation and thansfert the ownership of a project (from a user to the organisation).
- Linking the organisation's social media.