## webapp ci/cd

### The repositories
The project has 4 repositories belonging to a gitlab group:
- devpot-auth-service: contains backend code related to user registering/login.
- devpot-main-service: contains the code related to the backend.
- devpot-webapp: contains the code related to the frontend.
- devpot: contains the landing page and a docker-compose file used to run the whole project.

### The ci/cd stages
Each repository has its own ci/cd pipeline. For each repository, the ci/cd operations are defined in a .gitlab-ci.yml file that can be found in the root folder. When code is pushed, a process called the gitlab runner will look at this file, and execute the pipelines accordingly.
The ci/cd stages of the 3 services are similar and can be described as follows:
- a testing stage: this stage runs some unit tests against the corresponding service.
- a build stage: this stage is used to produce a docker image of the service. This newly generated image is then pushed to the docker registry of the gitlab group. The stage is run when a commit is pushed on master, or when a tagged commit is pushed on any branch.
- a deploy stage: this stage will deploy the project to a server.

### Example the main service ci/cd
As an example, here is the content of the .gitlab-ci.yml file found in the main service repository:
```
image: alpine:latest

stages:
   - test
   - build
   - deploy

test_main_service:
   stage: test
   image: node:latest
   variables:
      JWT_SECRET: $JWT_SECRET
      UPLOAD_PATH: files/images
   script:
      - npm install
      - npm test

build_main_service 0:
   stage: build
   image: docker:19.03.1
   services:
      - docker:19.03.1-dind
   script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      - docker pull $CI_REGISTRY_IMAGE:dev || true
      - docker build --cache-from $CI_REGISTRY_IMAGE:dev --tag $CI_REGISTRY_IMAGE:dev .
      - docker push $CI_REGISTRY_IMAGE
   only:
    - master

build_main_service 1:
   stage: build
   image: docker:19.03.1
   services:
      - docker:19.03.1-dind
   script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      - docker pull $CI_REGISTRY_IMAGE || true
      - docker build --cache-from $CI_REGISTRY_IMAGE --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --tag $CI_REGISTRY_IMAGE .
      - docker push $CI_REGISTRY_IMAGE
   only:
    - tags

deploy_to_droplet:
  stage: deploy
  image: node
  when: manual
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y  )'
    - eval $(ssh-agent -s)
    # Inject the remote's private key
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Append keyscan output into known hosts
    - ssh-keyscan $SERVER_IP >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ssh $SERVER_USER@$SERVER_IP './deploy.sh'
  only:
   - master  

```

We'll explain what happens during each stage.

#### the testing stage
This stage creates a docker container from the node latest docker image, and prepares it so that it contains the repository files. It sets some environnement variables that the service needs. It then runs what is found under scripts: it uses npm to install dependencies and run some unit tests.

```
test_main_service:
   stage: test
   image: node:latest
   variables:
      JWT_SECRET: $JWT_SECRET
      UPLOAD_PATH: files/images
   script:
      - npm install
      - npm test
```

#### the build stages
We do use the build stages to build a docker image of the project. This images will then be useful for developpers working on other services.
You'll see that there are two build stages. They are very similar, and the only difference is that they are triggered by different events: one is triggered every time on pushes some code on the master branch; this build the docker image tagged dev. The other is executed when a tagged commit is pushed. It build a docker image tagged with the tag used in the commit.
The images are then pushed to a gitlab docker registry that is shared by members of your group.
(Since the script uses the docker command, we use the service: docker-dind)
You'll notice that each new image will also overwrite the $CI_REGISTRY_IMAGE one. This used for performance reasons: each times we build an image we use cache from this image.
```
build_main_service 1:
   stage: build
   image: docker:19.03.1
   services:
      - docker:19.03.1-dind
   script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      - docker pull $CI_REGISTRY_IMAGE || true
      - docker build --cache-from $CI_REGISTRY_IMAGE --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG --tag $CI_REGISTRY_IMAGE .
      - docker push $CI_REGISTRY_IMAGE
   only:
    - tags
```

#### the deploy stage
This stage deploy the application on a droplet. It connect using ssh to a remote server, and execute a deploy.sh script found on the server. The script will then pull the latest image from our gitlab registry, stop the old container and start a new one.
```
deploy_to_droplet:
  stage: deploy
  image: node
  when: manual
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y  )'
    - eval $(ssh-agent -s)
    # Inject the remote's private key
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Append keyscan output into known hosts
    - ssh-keyscan $SERVER_IP >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ssh $SERVER_USER@$SERVER_IP './deploy.sh'
  only:
   - master  
```
