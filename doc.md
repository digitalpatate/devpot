![Screenshot 2019 09 18 At 23.21.17](doc-images/Logo-8995833.png)

Devpot regroups a ton of different software and video game devlogs. It enable users to follow their already loved projects and it makes it easier to discover new ones. No more browsing through various websites to find out the new updates. No more hours of browsing all around the web to encounter awesome new projects. One website, one place for software hunters or avid game followers.

Devpot is the perfect place for devlogs. But, what is a devlog you say? Devlogs are like posts in a forum but specialized in software or game development. For instance, when you, a game developer, finished a new update you might want to show people what's new and how you made it possible. A devlog can be very technical and explain implementation details like algorithms. But it may also enphasise on tales and stories. At the end, it's a nice and easy way to show your loved users and players what's new and how it became real.

Devpot is the perfect place for your devlogs. Sounds good now that you know what a devlogs is. Right?

On the main pages of the website we can find the projects and devlogs. This is the place where everyone can browse through the different projects available and read the corresponding devlogs.

Create yourself an account and got to comment fury. If you fin an awesome project, mark it as favorite not to lose it ever again. Every devlog needs reviews. Go and comment what you liked or not. Ask for questions or give advices.

While all this is epic, we still need content right? Everyone is welcome to create a project. Go to your dashboard and create an amazing project. Now that you project looks good, add devlogs and wait for awesome comments.