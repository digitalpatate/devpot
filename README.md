# Devpot

> PAGE Didier, MORENO Andres, BUDRY Nohan, STALDER Nicodeme

## General Aspect of the project 

This project consisted on creating a developing log portal for game and software developers. The main aspect of this portal is to enable users to share their new updates and features without the need of owning a website. 

##  Motivation

We've realized that by creating a developing log portal, we would be able to unify the different dev projetcs into a single place where the regular fan/gamer/user could check regularly without the need of checking each and every developer's website for updates. In other words, the time spent looking for news will be reduced.

## Features

* Register and sign in.
* Create projects.
* Users can manage multiple projects.
* Browse through projects and organizations.
* Mark projects as favourite.
* Comments projects. 
* Publish, edit and unpublish projects or devlogs.
* Phone responsive. 

## Microservices 

In a vision to separate our application, we've created different services: one for the [front-end](https://gitlab.com/digitalpatate/devpot-webapp), one for the [back-end](https://gitlab.com/digitalpatate/devpot-main-service) and one for the [authentication](https://gitlab.com/digitalpatate/devpot-auth-service). 

Dev technologies :
 - NodeJS.
 - MongoDB.
 - Mongoose.
 - ExpressJS.
 - JWT token based authentication system.
 - VueJS.
 - TailwindCSS.
 - Docker, docker-compose and traefik.

## Useful links

- Landing page: [digitalpatate.gitlab.io/devpot/](https://digitalpatate.gitlab.io/devpot)
- Devpot: [devpot.jackeri.ch](https://devpot.jackeri.ch)

## Installation

### Docker-compose

1. Open the  `docker-compose` folder.
2. Update the `auth-service.env` , `main-service.env` and `.env` file if needed. Default configuration should work just fine.
3. `.env` values :
   - MAINTAG: The tag of the backend, you can have a look of all possible values at https://gitlab.com/digitalpatate/devpot-main-service/container_registry . 
   - AUTHTAG: The tag of the auth service, you can have a look of all possible values at https://gitlab.com/digitalpatate/devpot-auth-service/container_registry 
   - FRONTTAG: The tag of the frontend app, you can have a look of all possible values at  https://gitlab.com/digitalpatate/devpot-webapp/container_registry .
   - SERVERURL: The actual server hostname. Used by the traefik to route the requests.
4. Run `docker-compose up`
5. Open your favorite browser and go to `localhost`

#### Https

Also at the repository, we can find a `docker-compose_prod.yml` file containing the *traefik* configuration. The reason for this, is that the browser returns a *self signed certificate* error that will block the connection to the reverse proxy (when running it on the same computer that has started the docker-compose). 